
// ==== String Problem #1 ====
// There are numbers that are stored in the format "$100.45", "$1,002.22", "-$123", and so on. Write a function to convert the given strings into their equivalent numeric format without any precision loss - 100.45, 1002.22, -123 and so on. There could be typing mistakes in the string so if the number is invalid, return 0. 





//==============================================================================================


const string = "$10010.045";






function problem1(string) {

    string = string.trim();

    if (string.includes("$") === false || string.match(/[$]/g).length > 1) {
        return 0;
    }

    
    if (string.match(/[.]/g)){
        if (string.match(/[.]/g).length > 1) {
            return 0;
        }
    }

    if(string[0] >= 0 && string[1] === "$"){
        return 0;
    }

    for (let index = 0; index < string.length; index++) {
        
        const currentElementData = string[index].match(/[0-9,.$+-]/g);
        if (!currentElementData) {
            return 0;
        }

        const currentElement = currentElementData[0];
        if (index > 1 && (currentElement === "$" || currentElement === "+" || currentElement === "-")) {
            return 0;
        }
    }


    let digit = string.split("$").join("");
    digit = digit.split(",").join("");
    digit = Number(digit);

    return digit;




}


// console.log(problem1(string));





module.exports = problem1;









