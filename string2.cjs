
// ==== String Problem #2 ====
// Given an IP address - "111.139.161.143". Split it into its component parts 111, 139, 161, 143 and return it in an array in numeric values. [111, 139, 161, 143].


// Support IPV4 addresses only. If there are other characters detected, return an empty array.



//==============================================================================================

// const IP = "111.99.161.143";

function problem2(IP){
    
    const array = IP.split(".");
    if(array.length > 4){
        return [];
    }

    for(let index = 0; index < array.length; index++){
        if(isNaN(array[index]) === true || array[index] < 0 || array[index] > 255 || array[index].length>3){
            return [];
        }
        const string = array[index];
        if(string.length===3){
            if(string[0] === "0"){
                return [];
            }
            if( string[0] === "0" && string[1] === "0" && string[2] > 0){
                return [];
            }
        }

        if(string.length === 2 && string[0] === "0"){
            return [];
        }

        array[index] = Number(array[index]);
            
        
    }

    return array;
}

// console.log(problem2(IP));


module.exports = problem2;








