

// ==== String Problem #3 ====
// Given a string in the format of "20/1/2021", print the month in which the date is present in.



//==================================================================================================

// const string = "20/04/2021";


function problem3(string) {
    for (let index = 0; index < string.length; index++) {
        const format = string[index].match(/[0-9/]/g);
        // console.log(format);
        if (format === null) {
            return;
        }
    }

    const date = string.split("/");
    if (date.length !== 3 || date[0] > 31) {
        return;
    }

    const monthNumber = Number(date[1]) - 1;
    const todayDate = Number(date[0]);
    const year = Number(date[2]);
    // console.log(todayDate);

    if (monthNumber >= 12 || monthNumber < 0 || todayDate <= 0 || todayDate > 31 || year === 0) {
        return;
    }

    const monthsArr = ["January", "Februarry", "March", "April", "May", "June", "July", 'August', "September", "October", "November", "December"];

    const monthsDateObj = {
        January: 31,
        Februarry: 28,
        March: 31,
        April: 30,
        May: 31,
        June: 30,
        July: 31,
        August: 31,
        September: 30,
        October: 31,
        November: 30,
        December: 31
    };

    const month = monthsArr[monthNumber];


    // if year is a leap year.
    if (year % 4 === 0 && year % 100 !== 0) {
        monthsDateObj.Februarry = 29;
    }

    if (todayDate > monthsDateObj[month]) {
        return;
    }

    return month;

}


// console.log(problem3(string));

module.exports = problem3;

