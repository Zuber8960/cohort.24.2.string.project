
// ==== String Problem #4 ====
// Given an object in the following format, return the full name in title case.
// {"first_name": "JoHN", "last_name": "SMith"}
// {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}



//===============================================================================================

// const obj = {"first_name": "JoHN", "middle_name": '   ', "last_name": "SMith" };


function problem4(obj) {
    // const name = "zUbER";
    function upperCase(name) {
        let word = "";

        name = name.split("");
        for (let index = 0; index < name.length; index++) {
            if (name.indexOf(name[index]) === 0) {
                word += name[index].toUpperCase();
            } else {
                word += name[index].toLowerCase();
            }
        }

        return word;
    }

    const values = Object.values(obj);
    // console.log(values);

    const vallidTitleCaseArray = [];
    for (let index = 0; index < values.length; index++) {
        if (values[index] != false) {
            vallidTitleCaseArray.push(values[index]);
        }
    }

    let titleCase = "";

    for (let index = 0; index < vallidTitleCaseArray.length; index++) {
        titleCase += upperCase(vallidTitleCaseArray[index]) + " ";
    }

    titleCase = titleCase.trim();


    return titleCase;

}


// console.log(problem4(obj));





module.exports = problem4;
















