
// ==== String Problem #5 ====
// Given an array of strings ["the", "quick", "brown", "fox"], convert it into a string "the quick brown fox."
// If the array is empty, return an empty string.





//=================================================================================================

// const array = ["the", "quick", " " ,"brown", "fox", " "];

function problem5(array){
    if(array.length === 0){
        return "";
    }

    const newArray = [];
    for(let index=0; index < array.length; index++){
        if(array[index] != false){
            newArray.push(array[index]);
        }
    }
    
    // console.log(newArray.length);

    return newArray.join(" ");
}



// console.log(problem5(array));


module.exports = problem5;











